getData_trans <- reactive({

  inFile <- input$df_trans

  if (is.null(input$df_trans))
    return(NULL)

  read.csv(inFile$datapath, header=input$header_trans, sep=input$sep_trans,
           quote=input$quote_trans)


})

output$contents_trans <- renderTable({

  lala_trans <- getData_trans()

  if(input$disp_trans == "head") {
    return(head(lala_trans))
  }
  else {
    return(lala_trans)
  }

})

output$trans_trans <- renderTable({

  mod_trans <- transforming(df = getData_trans(), var1 = input$var1_trans, var2 = input$var2_trans, var3 = input$var3_trans,
                            var4 = input$var4_trans, var5 = input$var5_trans, var6 = input$var6_trans, dvrt = input$dvrt_trans,
                            dver = input$dver_trans, incorr = input$incorr_trans, trans = input$trans_trans, subj = input$subj_trans,
                            medianmean = input$medianmean_trans, exclude = input$exclude_trans)




  if(input$disp_trans == "head") {
    return(head(mod_trans))
  }
  else {
    return(mod_trans)
  }

})

output$downloadData_trans <- downloadHandler(


  filename = function() {
    paste("data-transformed", Sys.Date(), ".csv", sep="")
  },
  content = function(file) {
    write.csv(transforming(df = getData_trans(), var1 = input$var1_trans, var2 = input$var2_trans, var3 = input$var3_trans,
                           var4 = input$var4_trans, var5 = input$var5_trans, var6 = input$var6_trans, dvrt = input$dvrt_trans,
                           dver = input$dver_trans, incorr = input$incorr_trans, trans = input$trans_trans, subj = input$subj_trans,
                           medianmean = input$medianmean_trans, exclude = input$exclude_trans),
              file, row.names = FALSE)
  }
)
