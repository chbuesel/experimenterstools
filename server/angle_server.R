output$angle_res <- renderText({
  
  visualAngle(size = as.numeric(input$isize), distance = as.numeric(input$idistance), angle = as.numeric(input$iangle))
  
})