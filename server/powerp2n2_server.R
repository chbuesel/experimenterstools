output$c_h3 <- renderText({
  
  calc_h(as.numeric(input$p1_p3), as.numeric(input$p2_p3))
  
})  

output$p_p3_res <- renderText({
  
  p2n2test_power(h = as.numeric(input$h_p3), n1 = as.numeric(input$n1_p3), n2 = as.numeric(input$n2_p3), sig.level = input$a_p3, power = input$p_p3, 
                 alternative = input$hyp_p3) 
  
})