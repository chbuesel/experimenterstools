fluidPage(
  
  tags$style(type="text/css",
             ".shiny-output-error { visibility: hidden; }",
             ".shiny-output-error:before { visibility: hidden; }"),
  
  
  setBackgroundColor(color = rgb(200, 200, 200, maxColorValue = 255)),
  
  h4("Convert Hex to RGB and HSV", align="center"),
  p("Enter the Hex-value you wish to convert. The remaining values will be displayed below in the Results.",
  "Alternatively, you can copy-and-paste the R- or Python-Function provided below in your preferred IDE", align="center"),
  br(),
  hr(),
  
  textInput(inputId = "iHex", label = "Hex-Value", placeholder = "e.g., #ff0000"), 
  
  br(),
  hr(),
  
  h4("Result:"),
  textOutput("line1hex"),
  textOutput("line2hex"),
  textOutput("line3hex"),
  
  br(),
  
  plotOutput("patch2"),
  
  br(),
  
  hr(),
  
  h4(icon(name = "r-project"),"-Function"),
  
  code("library(grDevices)", br(),
  " col2rgb(YourHexValue)"), br()
  
)