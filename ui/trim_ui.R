fluidPage(
  tags$style(type="text/css",
             ".shiny-output-error { visibility: hidden; }",
             ".shiny-output-error:before { visibility: hidden; }"
  ),
  
  setBackgroundColor(color = rgb(200, 200, 200, maxColorValue = 255)),
  
  
  # Title
  titlePanel("Trim Your Data"),
  helpText("With this tool, you can trim your data according to fix cut-off values or standard deviations.", br(),
           "1. Upload your data.", br(),
           "2. Specify your dependent variable (e.g. \"response_time\").", br(),
           "3. Choose whether you want to trim your data according to fixed cut-off values or standard deviations", br(),
           HTML('&emsp;'), "3.1. If you chose \"Fixed Cut-Off\", specify the upper and lower limits", br(),
           HTML('&emsp;'), "3.2. If you chose \"Standard Deviation\":", br(),
           HTML('&emsp;'), HTML('&emsp;'), "3.2.1. Specify up to seven conditions of which condition means/medians and SDs should be calculated", br(),
           HTML('&emsp;'), HTML('&emsp;'), "If you want to aggregate by subjects and conditions, enter subject as a condition.", br(),
           "4. Specify whether you want to use condition means or medians.", br(),
           "5. Specify how many SDs the data points are allowed to deviate from the condition means/medians.", br(),
           "6. Download your new dataframe by clicking the Download-Button.", br(),
           tags$h3("Step 1: Upload Your Data", align = "center")),
  fileInput("df_trim", "Choose File", multiple = TRUE, accept = c("text/csv", "text/comma-separated-values,text/plain", ".csv")),
  helpText("Only .csv-files are accepted."),
  helpText("The maximum file size is 30MB."),
  
  fluidRow(
    column(width = 3, checkboxInput("header_trim", "Header", TRUE)),
    column(width = 3, radioButtons("sep_trim", "Separator", choices = c(Comma = ",", Semicolon = ";", Tab = "\t"), selected = ",")),
    column(width = 3, radioButtons("quote_trim", "Quote", choices = c(None = "", "Double Quote" = '"', "Single Quote" = "'"), selected = '"')),
    column(width = 3, radioButtons("disp_trim", "Display", choices = c(Head = "head", All = "all"), selected = "head"))
  ),
  # Display outputs
  
  h4("Original Dataframe", align = "center"),
  # Output: Original Data
  tableOutput("contents_trim"),
  
  helpText("Only the first few rows of your dataframe are shown..."),
  
  tags$h3("Step 2: Trimming Procedure", align = "center"),

  fluidRow(
    column(width = 4, textInput(inputId = "dv_trim", label = "What is your dependent variable?", value = "", placeholder = "e.g., response_time"))
  ),
    
  radioButtons(inputId = "proc_trim", label = "Fixed or SDs?", choices = c("Fixed Cut-Offs" = "cutoff", "Standard Deviation" = "SD"), 
               selected = "cutoff"),

  tags$h4("Step 2a: If \"Fixed Cut-Offs\", choose cut-off values."),
  
  fluidRow(
    column(width = 2, textInput(inputId = "colower_trim", label = "Lower Cut-Off", value = "", placeholder = "e.g. 300")),
    column(width = 2, textInput(inputId = "coupper_trim", label = "Upper Cut-Off", value = "", placeholder = "e.g. 1500"))
  ),
  
  
  tags$h4("Step 2b: If \"Standard Deviation\":"),
  helpText("Choose whether deviations from the median or the mean should be used, how many SDs the values are allowed to deviate.", br(),
           "Next you can specify according to which factors the means/medians and SDs should be calculated and trimmed. Leave the variable
           fields empty if you want to trim according to the overall mean/median and SD"),  
  
  radioButtons(inputId = "meanmedian_trim", label = "Mean or Median?", choices = c("Mean" = "mean", "Median" = "median"), 
               selected = "mean"),
  
  textInput(inputId = "sds_trim", label = "Number of SDs", value = "", placeholder = "e.g. 2.5"),
  
  fluidRow(
    column(width = 2, textInput(inputId = "var1_trim", label = "Variable 1", value = "", placeholder = "e.g. subject_nr")),
    column(width = 2, textInput(inputId = "var2_trim", label = "Variable 2", value = "")),
    column(width = 2, textInput(inputId = "var3_trim", label = "Variable 3", value = "")),
    column(width = 2, textInput(inputId = "var4_trim", label = "Variable 4", value = ""))
  ),
  
  fluidRow(
    column(width = 2, textInput(inputId = "var5_trim", label = "Variable 5", value = "")),
    column(width = 2, textInput(inputId = "var6_trim", label = "Variable 6", value = "")),
    column(width = 2, textInput(inputId = "var7_trim", label = "Variable 7", value = ""))
  ),
  
  
  
  h4("Trimmed Dataframe", align = "center"),
  
  tableOutput("trimmed_trim"),
  
  tags$b("Data lost due to trimming in percent:"),
  
  textOutput("dataloss"),
  
  tags$h3("Step 3: Download your trimmed dataframe", align = "center"),
  
  downloadButton("downloadData_trim", "Download trimmed Data"), hr()
  
)