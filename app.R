library(shiny)
library(shinyWidgets)
library(ggplot2)
library(dplyr)
library(reshape2)

#increase maximum size for file upload
options(shiny.maxRequestSize = 30*1024^2)

#Load functions
source("functions/colorConversions.R")
source("functions/powerAnalyses.R")
source("functions/visualAngle.R")
source("functions/variousFunctions.R")
source("functions/esCalc.R")
source("functions/dataManipulation.R")



#Define UI (Tab layout)
ui<- fluidPage( 
  #for greek letters
  withMathJax(),
  
  tags$style(HTML("
      @import url('//https://fonts.googleapis.com/css?family=Ubuntu&display=swap');
      code {
        font-family: 'Ubuntu', sans-serif;
        color: #000000;
        background-color: #c8c8c8;
      }

    ")),
  
  tags$style(HTML("
      @import url('https://fonts.googleapis.com/css?family=Source+Sans+Pro&display=swap');
      p {
       font-family: 'Source Sans Pro', sans-serif;
        color: #000000;
      }

    ")),
  
  tags$style(HTML("
      @import url('//fonts.googleapis.com/css?family=Baloo+Da&display=swap');

    ")),
  
  tags$style(HTML("

      h1 {
        font-family: 'Baloo Da', cursive;
        color: #151515;
      }

    ")),
  tags$style(HTML("

      h2 {
        font-family: 'Baloo Da', cursive;
        color: #151515;
      }

    ")),  
  tags$style(HTML("

      h3 {
        font-family: 'Baloo Da', cursive;
        color: #151515;
      }

    ")),
  tags$style(HTML("

      h4 {
        font-family: 'Baloo Da', cursive;
        color: #151515;
      }

    ")),
  
  
  navbarPage("Experimenters' Toolbox",
             tabPanel("Start Page", icon = icon("hand-spock"),
                      source("ui/welcome_ui.R", local = TRUE)$value),
                
             tabPanel("Visual Angle", icon = icon("chart-area"),
                      source("ui/angle_ui.R", local = TRUE)$value),
                
             navbarMenu("Colors", icon = icon("palette"),
                           
                      tabPanel("Color Measurement", icon = icon("ruler"),
                               source("ui/measurement_ui.R", local = TRUE)$value),
                      
                      "Color Conversion",
                      tabPanel("RGB to Hex & HSV", icon = icon("sync"),
                               source("ui/rgb_ui.R", local = TRUE)$value),
                           
                      tabPanel("Hex to RGB & HSV", icon = icon("sync"),
                               source("ui/hex_ui.R", local = TRUE)$value)),
                
              navbarMenu("Power Analyses", icon = icon("charging-station"), 
                           
                      tabPanel("Correlation",
                               source("ui/powerr_ui.R", local = TRUE)$value),
                           
                      tabPanel("\\( \\chi^2 \\)-Squared Test",
                               source("ui/powerchi_ui.R", local = TRUE)$value),
                           
                      "t-Tests",
                      tabPanel("t-Test Equal Sample Size(s)",
                               source("ui/powert_ui.R", local = TRUE)$value),
                           
                      tabPanel("t-Test Unequal Sample Sizes",
                               source("ui/powert2_ui.R", local = TRUE)$value),
                           
                      "Proportion Tests",
                      tabPanel("Proportion Tests (one sample)",
                               source("ui/powerp_ui.R", local = TRUE)$value),
                           
                      tabPanel("Proportion Tests (two samples, equal N)",
                               source("ui/powerp2_ui.R", local = TRUE)$value),
                           
                      tabPanel("Proportion Tests (two samples, unequal N)",
                               source("ui/powerp2n2_ui.R", local = TRUE)$value)),
                
              navbarMenu("Effect Sizes", icon = icon("calculator"),
                      tabPanel("ES Calculations Between-Subjects Desigs",
                               source("ui/escalcBetween_ui.R", local = TRUE)$value),
                           
                      tabPanel("ES Calculations Within-Subject Desigs",
                               source("ui/escalcWithin_ui.R", local = TRUE)$value),
                           
                      tabPanel("(partial-) \\( \\eta^2 \\)",
                               source("ui/escalcPeta_ui.R", local = TRUE)$value)),
                
              navbarMenu("Other useful tools", icon = icon("wrench"),
                      tabPanel("d Prime Calculator",
                               source("ui/dprime_ui.R", local = TRUE)$value),
                           
                      tabPanel("EZ-Diffusion Model",
                               source("ui/ezDiff_ui.R", local = TRUE)$value)),
             
             navbarMenu("Data Management", icon = icon("table"),
                        tabPanel("Create Subsets",
                                 source("ui/subset_ui.R", local = TRUE)$value),
                        tabPanel("Trim Your Data",
                                 source("ui/trim_ui.R", local = TRUE)$value),
                        tabPanel("Long-To-Wide Format (e.g. for JASP, jamovi, SPSS)",
                                 source("ui/transform_ui.R", local = TRUE)$value)),
             
             navbarMenu("Tutorials", icon = icon("chalkboard-teacher"),
                        tabPanel("OpenSesame",
                                 source("ui/download_ui.R", local = TRUE)$value)),
             
              tabPanel("Contribute", icon = icon("file-upload"),
                      source("ui/contribute_ui.R", local = TRUE)$value)),
                
                hr(),
                
            tags$footer("Experimenter's Tools was created and is maintained by Christian Büsel.", 
                        br(),
                        "Find me on", a("ResearchGate", href = "https://www.researchgate.net/profile/Christian_Buesel"),
                        "and", a("Gitlab", href = "https://gitlab.com/chbuesel"),
                        br(),
                        icon("creative-commons"), icon("creative-commons-by"), 
                        a("License", href="http://creativecommons.org/licenses/by/3.0/"),
                        align = "right", style = "
                                                  font-size:12px;
                                                  position:relative;
                                                  color: gray;
                                                  padding: 2%;"
                            
  )

)

#define the server
server <- function(input, output){
  
  source("server/angle_server.R", local = TRUE)$value
  
  source("server/measurement_server.R", local = TRUE)$value
  
  source("server/rgb_server.R", local = TRUE)$value
  
  source("server/hex_server.R", local = TRUE)$value
  
  source("server/powerr_server.R", local = TRUE)$value
 
  source("server/powerchi_server.R", local = TRUE)$value
  
  source("server/powert_server.R", local = TRUE)$value
  
  source("server/powert2_server.R", local = TRUE)$value
  
  source("server/powerp_server.R", local = TRUE)$value
  
  source("server/powerp2_server.R", local = TRUE)$value
  
  source("server/powerp2n2_server.R", local = TRUE)$value
  
  source("server/dprime_server.R", local = TRUE)$value
  
  source("server/ezDiff_server.R", local = TRUE)$value
  
  source("server/subset_server.R", local = TRUE)$value
  
  source("server/trim_server.R", local = TRUE)$value
  
  source("server/transform_server.R", local = TRUE)$value
  
  source("server/escalcBetween_server.R", local = TRUE)$value

  source("server/escalcWithin_server.R", local = TRUE)$value
  
  source("server/escalcPeta_server.R", local = TRUE)$value
    
}

shinyApp(ui = ui, server = server)
